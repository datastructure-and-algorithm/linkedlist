/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author ACER
 */
public class main {

    public static void main(String[] args) {
        DoublyLinkedList list = new DoublyLinkedList();
        list.addLast(10);
        list.addLast(9);

        list.addLast(4);
        list.addLast(15);
        list.addLast(2);

        list.addLast(5);
        list.addLast(7);

        System.out.println("After ADDLAST: ");
        list.traverse();

        System.out.println("After ADDFIRST 175:");
        list.addFirst(175);
        list.traverse();

        System.out.println("Remove Last");
        list.removeLast();
        list.traverse();

        System.out.println("Remove FIRST");
        list.removeFirst();
        list.traverse();
        
        System.out.println("REMOVE 15");
        list.removeObjectData(15);
        list.traverse();

        System.out.println("PEAK FIRST:");
        System.out.println(list.peekFirst());
        
        System.out.println("PEAK LAST:");
        System.out.println(list.peakLast());
        
    }
}
