/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ACER
 */
public class DoublyLinkedList<T> {

    Node<T> head;
    Node<T> tail;

    public DoublyLinkedList(Node<T> head, Node<T> tail) {
        this.head = head;
        this.tail = tail;
    }

    public DoublyLinkedList() {
    }

    public void clear() {
        Node<T> currentNode = head;
        while (currentNode != null) {
            Node<T> nextNode = currentNode.next;
            currentNode.next = null;
            currentNode.prev = null;
            currentNode = nextNode;
        }
        head = tail = null;
    }

    public boolean isEmpty() {
        return head == null;
    }

    public void addFirst(T element) {
        if (isEmpty()) {
            head = tail = new Node<>(element, null, null);
        } else {
            Node<T> p = new Node<>(element, null, head);
            head.prev = p;
            head = head.prev;
        }

    }

    public T peekFirst() {
        if (isEmpty()) {
            throw new RuntimeException("Empty Linked List");
        }
        return head.info;
    }

    public T removeFirst() {
        if (isEmpty()) {
            throw new RuntimeException("Empty Linked List");
        }
        T data = head.info;
        head = head.next;
        if (isEmpty()) {
            tail = null;
        } else {
            head.prev = null;
        }
        return data;
    }

    public void addLast(T element) {
        if (isEmpty()) {
            head = tail = new Node<>(element, null, null);
        } else {
            Node<T> p = new Node<>(element, tail, null);
            tail.next = p;
            tail = tail.next;
        }
    }

    public T peakLast() {
        if (isEmpty()) {
            throw new RuntimeException("Empty Linked List");
        }
        return tail.info;
    }

    public T removeLast() {
        if (isEmpty()) {
            throw new RuntimeException("Empty Linked List!");
        }
        T data = tail.info;
        tail = tail.prev;
        if (isEmpty()) {
            tail = null;
        } else {
            tail=tail.prev;
            tail.next = null;
        }

        return data;
    }

    public T removeNode(Node<T> itemNode) {
        if (itemNode.prev == null) {
            removeFirst();
        }
        if (itemNode.next == null) {
            removeLast();
        }

        itemNode.prev.next = itemNode.next; //gán lại bằng cách gán nốt tiếp theo của Prev là nốt Next
        itemNode.next.prev = itemNode.prev; // ngược lại

        T data = itemNode.info;

        //clear Data (optional)
        itemNode.info = null;
        itemNode.next = null;
        itemNode.prev = null;
        itemNode = null;

        return data;
    }

    public boolean removeObjectData(Object object) {
        Node<T> currentNode = head;

        if (object == null) {
            while (currentNode != null) {
                if (currentNode.info == null) {
                    removeNode(currentNode);
                    return true;
                }
                currentNode = currentNode.next;
            }
        } else {
            while (currentNode != null) {
                if (currentNode.info == object) {
                    removeNode(currentNode);
                    return true;
                }
                currentNode = currentNode.next;
            }
        }
        return false;
    }

    public void traverse() {
        if (isEmpty()) {
            System.out.println("[]");
            return;
        } else {
            System.out.printf("[ ");
            Node<T> currentNode = head;
            while (currentNode != null) {
                System.out.printf(currentNode.info + ", ");
                currentNode= currentNode.next;
            }
            System.out.println(" ]\n");
        }
    }

}
